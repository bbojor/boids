# Boids
Simple boid implementation featuring several additive behaviour rules, and two types of boids: predators and prey.
Implemented using the SDL library.

![Picture](./boids.png)
![Picture](./boids2.png)
