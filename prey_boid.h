#include "vec2.h"
#include "SDL2/SDL.h"
#include <vector>
#include "boid.h"

extern const int WINDOW_WIDTH;
extern const int WINDOW_HEIGHT;
extern int mouseX, mouseY;

class PreyBoid: public Boid
{
    
    public:
        void update(std::vector<Boid*> b);
        void render(SDL_Renderer* renderer);
        PreyBoid();
        PreyBoid(int x, int y);
   
    private:
        const int MINIMUM_DISTANCE = 50;
        const double TOGETHERNESS = 1/(double)1000;
        const float VELOCITY_MAX = 5; 
        const double ALIGNMENT = 0.001;
        const int SIGHT_RANGE = 150;

        vec::vec2 centerOfMassRule(std::vector<Boid*> b);
        vec::vec2 distanceRule(std::vector<Boid*> b);
        vec::vec2 consistentVelocityRule(std::vector<Boid*> b);
        vec::vec2 goalRule(int goalX, int goalY);
        vec::vec2 onScreenRule();
        vec::vec2 runFromMouse();
        vec::vec2 runFromBoid(std::vector<Boid*> b);
        bool scared = false;
        unsigned int scare_timer = 0;
        const unsigned int SCARE_DURATION = 400;
        
        void limitVelocity();
};