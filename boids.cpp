#include <iostream>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>
#include "predator_boid.h"

const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 960;
int mouseX, mouseY;

 PredatorBoid* pred = NULL;

int main()
{
  
    if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        std::cout << "Failed to initialize SDL! Error:" << SDL_GetError() << std::endl;
        exit(1);
    }

    //create window
    SDL_Window *window = NULL;
    window = SDL_CreateWindow("Boids", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH,WINDOW_HEIGHT,SDL_WINDOW_SHOWN);
    if(window == NULL)
    {
        std::cout << "Failed to create window! Error:" << SDL_GetError() << std::endl;
        exit(1);
    }

    //create and color window surface
    SDL_Surface *surface = NULL;
    surface = SDL_GetWindowSurface(window);
    SDL_FillRect(surface, NULL,SDL_MapRGB(surface->format, 0x00, 0x00, 0x00));

    //create renderer
    SDL_Renderer *renderer = NULL;
    renderer = SDL_GetRenderer(window);
    if(renderer == NULL)
    {
        std::cout << "Failed to create renderer! Error:" << SDL_GetError() << std::endl;
        exit(1);
    }


    std::vector<Boid*>  boids;
    
    SDL_Event event;
    bool running = true;

    std::cout << "Use \"+\" to add a prey boid and \"-\" to remove one.\nUse \"p\" to add a predator boid.\nBoids run away from the cursor.\nPress \"q\" to quit.\n";

    while(running)
    {
        SDL_SetRenderDrawColor(renderer,0,0,0,0); //clear with black
        SDL_RenderClear(renderer);

        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_KEYDOWN:
                    if(event.key.keysym.sym == SDLK_q)
                    {
                        running = false;
                    }

                    else if(event.key.keysym.sym == SDLK_EQUALS) //add a boid
                    {
                        int k = 3 - rand()%6;
                        PreyBoid* b = new PreyBoid(WINDOW_WIDTH/ 2 + k * 100, WINDOW_HEIGHT / 2 + k * 100);
                        boids.push_back(b);
                    }
                    else if(event.key.keysym.sym == SDLK_MINUS) //remove a boid
                    {
                        if(boids.size() > 0)
                            boids.pop_back();
                    }
                    else if(event.key.keysym.sym == SDLK_p)
                    {
                        if(!pred)
                        {
                            int k = 3 - rand()%6;
                            pred = new PredatorBoid(WINDOW_WIDTH/ 2 + k * 100, WINDOW_HEIGHT / 2 + k * 100);
                            boids.push_back(pred);
                        }        
                    }
                
                break;

                case SDL_MOUSEMOTION:
                    SDL_GetMouseState(&mouseX, &mouseY);
                     
            }
        }

        for(long unsigned int i = 0; i < boids.size(); i++)
        {
            if(boids.at(i)->shouldRemove())
            {
                boids.erase(boids.begin() + i);
                i--;
            }
            else
            {
                boids.at(i)->update(boids);
                boids.at(i)->render(renderer);
            }
        }

        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    }

    SDL_DestroyWindow(window);

    SDL_Quit();

    return 0;
}