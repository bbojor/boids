#include <iostream>
#include "predator_boid.h"
#include <vector>

using namespace vec;

void PreyBoid::update(std::vector<Boid*> b)
{   
   velocity = velocity + centerOfMassRule(b) + distanceRule(b) + Boid::onScreenRule() + consistentVelocityRule(b) - runFromMouse() - runFromBoid(b);
   Boid::limitVelocity();
   if(!scared)
        velocity += decelerate();
   else
   {
       scare_timer--;
       if(scare_timer ==  0)
        scared = false;
   }
   
   if(velocity.length() < 0.2)
    velocity = vec2(0,0);

   position += velocity;
   orientation = position + velocity;
}

//computes the center of mass of the other boids and moves the boid 1/100th of the distance
vec2 PreyBoid::centerOfMassRule(std::vector<Boid*> b)
{
    vec2 centerOfMass;
    int closestFlockSize = 0;

    for(long unsigned int i = 0; i< b.size(); i++)
        if(dynamic_cast<PreyBoid*>(b.at(i)) != nullptr)
        {
            if((b.at(i)->getPosition() - position).length() < SIGHT_RANGE)
            {   
                centerOfMass += b.at(i)->getPosition();
                closestFlockSize++;
            }
        } 

    if(closestFlockSize <= 1)
        return vec2(0,0);

    centerOfMass = (centerOfMass - position) * (1/(float)(closestFlockSize - 1));

    return (centerOfMass - position) * TOGETHERNESS ;
}

//tries to mantain a minimum distance between each two boids
vec2 PreyBoid::distanceRule(std::vector<Boid*> b)
{
   vec2 correction;

   for(unsigned long int i = 0; i < b.size(); i++)
   {
       if(dynamic_cast<PreyBoid*>(b.at(i)) != nullptr)
           if((position - b.at(i)->getPosition()).length() < MINIMUM_DISTANCE)
           {
              correction += (position - b.at(i)->getPosition());
           }
   }
   
   return correction * (1/(float)50);
}

//tries to get boids to have a consistent velocity with each other
vec2 PreyBoid::consistentVelocityRule(std::vector<Boid*> b)
{
    vec2 averageVelocity;
    int closestFlockSize = 0;

    for(unsigned long int i = 0; i< b.size(); i++)
    {
        if(dynamic_cast<PreyBoid*>(b.at(i)) != nullptr)
            if((b.at(i)->getPosition() - position).length() < SIGHT_RANGE)
            {
                averageVelocity += b.at(i)->getVelocity();
                closestFlockSize++;
            }
    }

    if(closestFlockSize <= 1)
    {
        return averageVelocity;
    }

    averageVelocity = (averageVelocity - velocity) *  (1 / (float)(closestFlockSize - 1));

    return (averageVelocity - velocity) * (1/(float)8);
}  

//makes boids run away from the mouse
vec2 PreyBoid::runFromMouse()
{
    vec2 adjustment =  vec2(mouseX, mouseY) - position;
    float distance = adjustment.length();

    if(distance < 200)
        return adjustment;
    else 
    return vec2(0,0);
}

vec2 PreyBoid::runFromBoid(std::vector<Boid*> b)
{
     vec2 adjustment;

    for(unsigned int i = 0; i < b.size(); i++)
    {
        if(dynamic_cast<PredatorBoid*>(b.at(i)) != nullptr)
        {
           vec2 dist = b.at(i)->getPosition() - position;
           float distance = dist.length();              

            if(distance < SIGHT_RANGE)
            {
                if(distance < 40)
                {
                    remove = true;
                }
                else
                {
                    adjustment += dist;
                    scared = true;
                    scare_timer = SCARE_DURATION;
                }
            }
        }
    }

    return adjustment;
}

PreyBoid::PreyBoid() : Boid()
{
   blue = 255;
}

PreyBoid::PreyBoid(int x, int y) : Boid(x, y) 
{
   blue = 255;
}



