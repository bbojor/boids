#include "vec2.h"
#include "SDL2/SDL.h"
#include <vector>

extern const int WINDOW_WIDTH;
extern const int WINDOW_HEIGHT;
extern int mouseX, mouseY;

class Boid 
{
    public: 
       
        Boid();
        Boid(int x, int y);

        void setPosition(int x, int y);
        void setOrientation(int x, int y);
        vec::vec2  getPosition();
        vec::vec2 getVelocity();
        void render(SDL_Renderer *renderer);
        virtual void update(std::vector<Boid*> b) = 0;
        bool shouldRemove();

    protected:

        vec::vec2 velocity;
        vec::vec2 position;
        vec::vec2 orientation;
        const int VELOCITY_MAX = 5;
        SDL_Point v1 = {5,5}, v2 = {10, 20}, v3 = {0, 20};
        int red = 0, green = 0, blue = 0, alpha = 0;;
        
        void fill(SDL_Renderer* renderer, SDL_Point v1, SDL_Point v2, SDL_Point v3);
        vec::vec2 goalRule(int goalX, int goalY);
        vec::vec2 onScreenRule();
        void limitVelocity();
        bool remove = false;
        vec::vec2 decelerate();
};