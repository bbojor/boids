#include "vec2.h"
#include "SDL2/SDL.h"
#include <vector>
#include <prey_boid.h>

extern const int WINDOW_WIDTH;
extern const int WINDOW_HEIGHT;

class PredatorBoid : public Boid
{
    public: 
      
        PredatorBoid();
        PredatorBoid(int x, int y);

   
        void update(std::vector<Boid*> b);

    private:
       
        const float VELOCITY_MAX = 5; 
        const int SIGHT_RANGE = 200;
       
        vec::vec2 findPrey(std::vector<Boid*> b);
};  