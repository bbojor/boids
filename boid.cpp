#include <iostream>
#include "prey_boid.h"
#include <vector>

using namespace vec;

void Boid::setPosition(int x, int y)
{
   position.x = x;
   position.y = y;
}

void Boid::setOrientation(int x, int y)
{
    orientation.x = x;
    orientation.y = y;
}

vec2 Boid::getPosition()
{
    return position;
}

vec2 Boid::getVelocity()
{
    return velocity;
}

//tries to keep boids on screen
vec2 Boid::onScreenRule()
{
    vec2 correction;

    if(position.x < 100)
        correction.x = 1;
    
    else if(position.x > WINDOW_WIDTH - 100)
        correction.x = -1;

    if(position.y < 100)
        correction.y = 1;
    
    else if(position.y > WINDOW_HEIGHT - 100)
        correction.y = -1;

    return correction;
}

//limits the velocity of boids
void Boid::limitVelocity()  
{
    if(velocity.length() > VELOCITY_MAX)
    {
        velocity  = (velocity * (1 / velocity.length())) * VELOCITY_MAX;
    }
}

//makes boids gravitate to given coordinates
vec2 Boid::goalRule(int goalX, int goalY)
{
    vec2 adjustment = vec2(goalX, goalY) - position;
    float distance = adjustment.length();

    return adjustment.normalize() * (distance/100);
}

vec2 Boid::decelerate()
{   
    return -velocity.normalize() * (1/(float)10000000000);
}

void Boid::render(SDL_Renderer *renderer)
{
    float direction_length = (position - orientation).length();

    double sin = 0, cos = 1;

    if(direction_length != 0)
    {
        sin = (orientation.x - position.x) / direction_length; 
        cos = (position.y - orientation.y) / direction_length; 
    }
    
    SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);
    
    //for a proper rotation we need to translate the triangle's center to the origin
    SDL_Point v_translate = {(v1.x + v2.x + v3.x) / 3, (v1.y + v2.y + v3.y) / 3};

    //compute vertices with offset and rotation
    SDL_Point 
     v1_current = {(int)((v1.x - v_translate.x) * cos - (v1.y - v_translate.y) * sin + v_translate.x + position.x), (int)((v1.y - v_translate.y) * cos + (v1.x - v_translate.x) * sin + v_translate.y + position.y)},
     v2_current = {(int)((v2.x - v_translate.x) * cos - (v2.y - v_translate.y) * sin + v_translate.x + position.x), (int)((v2.y - v_translate.y) * cos + (v2.x - v_translate.x) * sin + v_translate.y + position.y)}, 
     v3_current = {(int)((v3.x - v_translate.x) * cos - (v3.y - v_translate.y) * sin + v_translate.x + position.x), (int)((v3.y - v_translate.y) * cos + (v3.x - v_translate.x) * sin + v_translate.y + position.y)};
    
    fill(renderer, v1_current, v2_current, v3_current);
}

//fill triangle using baricentric coordinate rasterization
void Boid::fill(SDL_Renderer* renderer, SDL_Point v1, SDL_Point v2, SDL_Point v3)
{
    //compute bounding box of triangle
    int xmin = v1.x, xmax = v1.x, ymin = v1.y, ymax = v1.y;

    if(v2.x < xmin)
        xmin = v2.x;
    else 
    if(v2.x > xmax)
        xmax = v2.x;

    if(v3.x < xmin)
        xmin = v3.x;
    else 
    if(v3.x > xmax)
        xmax = v3.x;

    if(v2.y < ymin)
        ymin = v2.y;
    else 
    if(v2.y > ymax)
        ymax = v2.y;

    if(v3.y < ymin)
        ymin = v3.y;
    else 
    if(v3.y > ymax)
        ymax = v3.y;

    //iterate over points inside the bounding box and color the ones inside the triangle
    for(int i = xmin; i < xmax; i++)
        for(int j = ymin; j < ymax; j ++)
        {
            //compute baricentric coordinates of a point
            double alpha = ((v2.y - v3.y) * i + (v3.x - v2.x) * j + v2.x * v3.y - v3.x * v2.y) /(double) ((v2.y - v3.y) * v1.x + (v3.x - v2.x) * v1.y + v2.x * v3.y - v3.x * v2.y);

        	double beta  = ((v3.y - v1.y) * i + (v1.x - v3.x) * j + v1.y * v3.x - v1.x * v3.y) / (double)((v3.y - v1.y) * v2.x + (v1.x - v3.x) * v2.y + v1.y * v3.x - v1.x * v3.y);

        	double gamma = 1 - alpha - beta;

            if(alpha >= 0 && alpha <= 1 && beta >= 0 && beta <= 1 && gamma >= 0 && gamma <= 1)
                SDL_RenderDrawPoint(renderer, i, j);
        }
}

Boid::Boid()
{
    position = vec2(0,0);
    orientation = vec2(0,0);
    velocity = vec2(0,0);
}

Boid::Boid(int x, int y)
{
    position = vec2(x,y);
    orientation = vec2(0,0);
    velocity = vec2(0,0);
}

bool Boid::shouldRemove()
{
    return remove;
}