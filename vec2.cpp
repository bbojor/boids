#include<vec2.h>

using namespace vec;

    
    vec2& vec2::operator =(const vec2 &srcVector)
    {
        x = srcVector.x;
        y = srcVector.y;

        return *this;
    }

    vec2 vec2::operator +(const vec2& srcVector) const
    {
         vec2 result;
         result.x = x + srcVector.x;
         result.y = y + srcVector.y;

         return result;
    }

    vec2& vec2::operator +=(const vec2& srcVector)
    {
        x += srcVector.x;
        y += srcVector.y;

        return *this;
    }

    vec2 vec2::operator *(float scalarValue) const
    {
        vec2 result;
        result.x = x * scalarValue;
        result.y = y * scalarValue;

        return result;
    }

    vec2 vec2::operator -(const vec2& srcVector) const
    {
        vec2 result;
        result.x = x - srcVector.x;
        result.y = y - srcVector.y;

        return result;
    }

    vec2& vec2::operator -=(const vec2& srcVector)
    {
        x -= srcVector.x;
        y -= srcVector.y;

        return *this;
    }

    vec2 vec2::operator -() const
    {
        vec2 result;
        result.x = -x;
        result.y = -y;

        return result;
    }

    float vec2::length() const
    {
        return sqrt(x*x + y*y);
    }

    vec2& vec2::normalize()
    {
        if(length() != 0)
        {
            x /= length();
            y /= length();
        }
        else
        {
            x = 0;
            y = 0;
        }
        
      
        return *this;
    }
