#include <iostream>
#include "predator_boid.h"
#include <vector>
#include <limits>

using namespace vec;

void PredatorBoid::update(std::vector<Boid*> b)
{

   velocity = velocity + onScreenRule() + findPrey(b) ;
   limitVelocity();
   position += velocity;
   orientation = position + velocity;
}

vec2 PredatorBoid::findPrey(std::vector<Boid*> b)
{
    vec2 target = vec2(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity());

    for(unsigned int i =0; i< b.size(); i++)
    {
        vec2 distance;
        distance = position - b.at(i)->getPosition();
        if(distance.length() < SIGHT_RANGE && distance.length() > 0 && distance.length() < (position - target).length())
        {
            target = b.at(i)->getPosition();
        }
    }
    if(target.x == std::numeric_limits<float>::infinity() || target.y == std::numeric_limits<float>::infinity())
        return vec2(0,0);

    return -(position - target).normalize() * 0.7;
}

PredatorBoid::PredatorBoid() : Boid()
{
   red = 255;
   v2 = {20 ,40};
   v3 = {-10 ,40};
}

PredatorBoid::PredatorBoid(int x, int y) : Boid(x, y)
{
  red = 255;
  v2 = {20 ,40};
  v3 = {-10 ,40};
}